import React, { useState, useEffect } from "react";

const CatProfile = () => {
    const [cats, setCats] = useState([]);

    useEffect(() => {
    //   fetch("http://localhost:3001/api/cats")
    //     .then((res) => res.json())
    //     .then((res) => this.setCats(res));
    // });

     async function fetchData() {
      const res = await fetch("http://localhost:3001/api/cats");
      res
        .json()
        .then((res) => setCats(res))
        //.catch((err) => setErrors(err));
    }
    fetchData();
  }, []);

  return (
    <div>
      {cats.map((cat) => (
        <div key={cat.id}>
          <p>{cat.name}</p>
          <img src={cat.image} alt={cat.name} width="300" height="300" />
        </div>
      ))}
    </div>
  );
};

export default CatProfile;
